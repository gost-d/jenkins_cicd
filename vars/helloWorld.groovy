def call() {
    
    pipeline {

    agent any 

    stages {

        stage ('Checkout') {
            
            steps { checkout scm  }
            when { changeset "openshift/template.yaml" } 
        }

        stage ("Echo hello") {
           steps { 
               sh 'oc login --token --server' 
               sh ' echo "oc process -f openshift/template.yaml | oc create -n thor-dmz-dev1 -f -"'
            }
        }
    }
}
}
